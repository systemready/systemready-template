# This template has been moved

The SystemReady IR template has been moved to the [systemready-ir-template] repository.

[systemready-ir-template]: https://gitlab.arm.com/systemready/systemready-ir-template
